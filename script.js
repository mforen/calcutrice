// Sélectionnez l'élément d'affichage (input) de la calculatrice
const result = document.getElementById("result");

// Sélectionnez tous les boutons de la calculatrice
const buttons = document.querySelectorAll(".buttons button");

// Ajoutez un gestionnaire d'événements à chaque bouton
buttons.forEach((button) => {
    button.addEventListener("click", () => {
        // Récupérez le texte du bouton
        const buttonText = button.textContent;

        // Si le bouton est "C", effacez le dernier caractère de l'input
        if (buttonText === "C") {
            result.value = result.value.slice(0, -1);
        }
        // Si le bouton est "=", évaluez l'expression et affichez le résultat
        else if (buttonText === "=") {
            try {
                result.value = eval(result.value);
            } catch (error) {
                result.value = "Erreur";
            }
        } else {
            // Sinon, ajoutez le texte du bouton à l'input
            result.value += buttonText;
        }
    });
});
